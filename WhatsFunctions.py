from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from collections import OrderedDict
import time

class Whatsapp(object):
	def __init__(self):
		self.options = webdriver.ChromeOptions()
		# self.options.add_argument('--headless')
		self.options.add_argument('--profile-directory=Default')
		self.options.add_argument('--user-data-dir=./User_Data')
		self.options.add_argument("--window-size=1920x1080")
		self.driver = webdriver.Chrome(chrome_options=self.options)
		# self.driver.maximize_window()
		self.driver.get('https://web.whatsapp.com/')
		print('Iniciando bot!')
		time.sleep(25)

	def find_contact(self, contato):
		for self.painel in self.driver.find_elements_by_tag_name('span'):
			self.nome_contato = self.painel.get_attribute('title')
			if self.nome_contato.lower().replace(' ','').replace('-','') == contato.lower().replace(' ','').replace('-',''):
				self.painel.click()
				break

	def check_contact(self):
		for self.painel in self.driver.find_elements_by_class_name('_2WP9Q'):
			self.nome_contato = self.painel.find_element_by_css_selector('div.KgevS')
			self.nome_contato = self.nome_contato.find_element_by_tag_name('span')
			try:
				self.btn_verdinho = self.painel.find_element_by_class_name('P6z4j')
				self.btn_verdinho_text = (self.btn_verdinho.text)
				self.btn_verdinho = True
				break
			except:
				self.btn_verdinho = False
		if self.btn_verdinho == True:
			return [self.nome_contato, True]
		return [self.nome_contato, False]

	def get_message(self):
		list_messages = []
		list_msg_new = []
		self.all_messages = self.driver.find_elements_by_class_name('copyable-text')
		for self.message_element in self.all_messages:
			for self.message_text in self.message_element.find_elements_by_tag_name('span'):
				list_messages.append(self.message_text.text)
		return list_messages[-1]

	def send_message(self, message):
		self.text_box = self.driver.find_element_by_css_selector('div._3u328.copyable-text.selectable-text')
		for part in message.split('\n'):
			self.text_box.send_keys(part)
			ActionChains(self.driver).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).perform()
		self.driver.find_element_by_css_selector('button._3M-N-').click()

	def get_name(self, contato):
		self.find_contact(contato)
		self.header_name = self.driver.find_element_by_class_name('_1lpto').click()
		time.sleep(1)
		self.name = self.driver.find_element_by_class_name('_2he9-')
		self.driver.find_element_by_class_name('qfKkX').click()
		time.sleep(1)
		return self.name.text

	def wait_btn(self):
		self.find_contact('Espera')
		self.wait = WebDriverWait(self.driver, 199999999)
		self.men_menu = self.wait.until(ec.visibility_of_element_located((By.CLASS_NAME, "P6z4j")))
		self.find_contact('Espera')