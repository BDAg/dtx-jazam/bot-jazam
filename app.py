from flask import Flask, request, jsonify
from dbConnect import connect
from chatbot import Chat_bot
from datetime import datetime
from verifica_ausencia import get_number_days
import pymongo
import time
#inicializing bot+api
app = Flask(__name__)
conn, mydb = connect()

#enviar mensagem para um contato só
@app.route('/single_message', methods=['POST'])
def send_message_by_api():
    """
        Enviar status: "compra_enviada" (para primeiro momento),
        "compra_bem_sucedida" (quando precisar avisar que já está indo entregar)
    """
    status = request.json['status']
    message = request.json['message']
    contact = request.json["contato"].replace(' ', '').replace('-', '')
    chat.whatsapp.find_contact(contact)
    chat.whatsapp.send_message(message)
    date_status = datetime.today().strftime('%d/%m/%Y')
    if (request.json["lastPurchasesPrice"]):
        mydb.message_time.find_one_and_update(
            {
                "_id":str(contact)
            }, 
                {'$set': 
                    {
                        "lastPurchasesPrice":str(request.json["lastPurchasesPrice"]),
                        "status_compra":str(status),
                        "data_status":str(date_status)
                    }
                }
            )
    else:    
        mydb.message_time.find_one_and_update(
            {
                "_id":str(contact)
            }, 
                {'$set': 
                    {
                        "status_compra":str(status),
                        "data_status":str(date_status)
                    }
                }
            )
    return jsonify({"status":status})

#enviar mensagem para uma lista de contatos
@app.route('/promotes', methods=["POST"])
def send_messages_promotes():
    message = request.json['message']
    list_contacts = request.json['list_contacts']
    cont_contacts_sent = 0
    for contact in list_contacts:
        chat.whatsapp.find_contact(contact)
        chat.whatsapp.send_message(message)
        time.sleep(.1)
        cont_contacts_sent+=1
    
    return jsonify({
        "numero de contatos enviados": cont_contacts_sent,
        "message":message
    })

@app.route('/pos_venda_25_15', methods=["POST"])
def pos_25_15():
    message = request.json['message']
    list_contacts = request.json['list_contacts']
    print(list_contacts)
    print("-------------------")
    cont_contacts_sent = 0
    for x in range(0, len(list_contacts)):
        contact = list_contacts[x]["id"]
        lastContact25dias = list_contacts[x]["lastContact25"]
        chat.whatsapp.find_contact(contact)
        if lastContact25dias == False:
            chat.whatsapp.send_message(message[1])
            mydb.message_time.find_one_and_update(
            {
                "_id":str(contact)
            }, 
                {'$set': 
                    {
                        "lastContact25":True
                    }
                }
            )
        elif lastContact25dias == True:
            chat.whatsapp.send_message(message[0])
            mydb.message_time.find_one_and_update(
            {
                "_id":str(contact)
            }, 
                {'$set': 
                    {
                        "lastContact25":False
                    }
                }
            )
        cont_contacts_sent += 1
    return jsonify({
        "numero de contatos enviados": cont_contacts_sent,
        "message":message
    })

@app.route('/verify_days', methods=['GET'])
def verify_days():
    get_number = get_number_days()
    list_users = get_number.find_users_15_days()
    return jsonify({
        "list": list_users
    })


if __name__ == "__main__":
    app.run(debug=True)
    chat = Chat_bot()
    try:
        count = 0
        chat.main(count)
    except KeyboardInterrupt:
        chat.whatsapp.driver.close()
    except:
        chat.whatsapp.driver.close()
        time.sleep(5)
        count = 0
        chat.main(count)