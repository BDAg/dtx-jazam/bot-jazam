# -*- coding: utf-8 -*-
from connect_api import connection as conn
from WhatsFunctions import Whatsapp
import selenium.common.exceptions
from datetime import date
import pymongo
import dbConnect
import normaliza
import time
import json 
import sys
import os

class Chat_bot(object):
	def __init__(self):
		with open('words\choises.json', 'r', encoding='utf-8') as f:
			self.json_file = json.load(f)
		with open('words\products.json', 'r', encoding='utf-8') as file:
			self.json_file_products = json.load(file)
		self.connection, self.mydb = dbConnect.connect()
		self.conn_api = conn()
		self.whatsapp = Whatsapp()
	
	def process_message(self, message):
		try:
			message = normaliza.remover_acentos(message)
			message = normaliza.remove_pontuacao(message)
			self.split_msg = message.lower().split(' ')
			return self.split_msg
		except:
			message = normaliza.remover_acentos(message)
			message = normaliza.remove_pontuacao(message)
			return message
	
	def create_message(self, message, number):
		self.message_split = self.process_message(message)
		self.numero_sac = self.json_file['duvidas'][-1]
		for i in range(len(self.message_split)):
			for product in self.json_file_products:
				if self.message_split[i] in self.json_file_products[str(product)]:
					self.response_gera_link = self.conn_api.gera_link(number, self.message_split[i])
					print(self.response_gera_link)
					try:
						link = self.response_gera_link['link']
						return (f"Para ver mais sobre {self.message_split[i]}, acesse o site a baixo:\n\nSite: {link}")	
					except KeyError:
						return (f"O senhor(a) não está cadastrado no nosso sistema. Entre em contato conosco por meio do telefone ({self.numero_sac}) para de cadastrar!")
			for choise in self.json_file:
				if self.message_split[i] in self.json_file[choise]:
					self.response_gera_link = self.conn_api.gera_link(number)
					if choise == "inicia_conversa":
						return ('Olá, o senhor(a) gostaria de ver os nossos produtos? Digite "Produtos" ou o nome do produto que deseja!')
					try:
						link = self.response_gera_link['link']
						if choise == "produtos":
							return (f"Ótima escolha!!\nPara ver todos os {self.message_split[i]}, acesse o site a baixo:\n\nSite: {link}")
						elif choise == "agradecimentos":
							return ("De nada, fico muito feliz em poder ajudar!!\n\nVolte sempre para conversar comigo! :D")
						elif choise == "duvidas":
							return (f"O senhor(a) está com {self.message_split[i]}? Para podermos te atender melhor, entre em contato conosco por meio do telefone {self.numero_sac}")
					except KeyError:
						return (f"O senhor(a) não está cadastrado no nosso sistema. Entre em contato conosco por meio do telefone ({self.numero_sac}) para de cadastrar!")
	
	def get_response(self, message, number):
		self.message_created = self.create_message(message, number)
		if self.message_created == None:
			self.message_created = ("Desculpe, mas não entendi o que o senhor(a) deseja ._.'")
		self.whatsapp.send_message(self.message_created)
		print(self.message_created)

	def main(self,count):
		sys.setrecursionlimit(1999999999)
		try:
			while True:
				self.tem_conversa = self.whatsapp.check_contact()
				if self.tem_conversa[1] == True:
					self.whatsapp.find_contact(self.tem_conversa[0].text)
					print('entrou no contato:', self.tem_conversa[0].text)
					number = self.tem_conversa[0].text
					number = number.replace(' ','').replace('-','')
					msg = self.whatsapp.get_message()
					self.get_response(msg, number)
					current_day = date.today().strftime("%d/%m/%Y")
					try:
						self.mydb.message_time.insert_one({"_id":str(number), "last_message": str(current_day), "message":str(msg)})
						self.whatsapp.find_contact('Espera')
					except pymongo.errors.DuplicateKeyError:
						for self.doc in self.mydb.message_time.find({'_id':str(number)}):
							self.mydb.message_time.find_one_and_replace(self.doc, {"_id":str(number), "last_message": str(current_day), "message":str(msg)} )
							self.whatsapp.find_contact('Espera')
				elif self.tem_conversa[1] == False:
					try:
						self.whatsapp.wait_btn()
						print('ouvindo .',count)
						time.sleep(.1)
						count+=1
						self.main(count)
					except Exception as e:
						print(e)
						time.sleep(1)
						self.whatsapp.wait_btn()
						print('ouvindo .',count)
						time.sleep(.1)
						count+=1
						self.main(count)
				time.sleep(1)
		except selenium.common.exceptions.StaleElementReferenceException:
			self.main(count)
		except selenium.common.exceptions.InvalidSessionIdException:
			self.main(count)
		except RecursionError:
			self.whatsapp.driver.close()
			self.main(count)
		except KeyboardInterrupt:
			self.whatsapp.driver.close()

if __name__ == "__main__":    	
	chat = Chat_bot()
	try:
		count = 0
		chat.main(count)
	except KeyboardInterrupt:
		chat.whatsapp.driver.close()
	except selenium.common.exceptions.TimeoutException:
		chat.whatsapp.driver.close()
		time.sleep(5)
		count = 0
		chat.main(count)