import requests
import urllib.request as req
import json
class connection(object):
    def __init__(self):
        pass

    def gera_link(self, number, filter=None):
        if filter != None and filter != '':
            self.resp_token = requests.post(f'http://localhost:3000/login',{
                "number":number,
                "filter":filter
            })
            self.resp_token = self.resp_token.content
            self.json_content = json.loads(self.resp_token)
            return self.json_content
        self.resp_token = requests.post(f'http://localhost:3000/login',{
            "number":number  
        })
        self.resp_token = self.resp_token.content
        self.json_content = json.loads(self.resp_token)
        return self.json_content