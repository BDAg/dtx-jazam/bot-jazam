from unicodedata import normalize
import re

def remover_acentos(txt):
    return normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')

def remove_pontuacao(txt):
    new_word = re.sub(r'[?|!|.|,|%|@|#|¨|&]','',txt)
    return new_word    

if __name__ == '__main__':
    from doctest import testmod
    testmod()