from datetime import datetime, date
from dbConnect import connect
import time

class get_number_days(object):
    def __init__(self,):
        self.conn, self.mydb = connect()

    def diff_days(self,date2):
        d1 = datetime.today()
        d2 = datetime.strptime(date2, "%d-%m-%Y")
        return abs((d2 - d1).days)

    def find_users_15_days(self):
        self.users = self.mydb.message_time.find()
        self.user_list = []
        for self.user in self.users:
            try:
                date = self.user['last_message'].replace('/','-')
                date_sale = self.user['data_status'].split(' ')
                if self.diff_days(date) == 25 or self.diff_days(str(date_sale[0])) == 25:
                    self.user_list.append({
                        "id":self.user['_id'],
                        "lastContact25": self.user["lastContact25"]
                        })
                elif self.diff_days(date) == 15 or self.diff_days(str(date_sale[0])) == 15:
                    self.user_list.append({
                        "id":self.user['_id'],
                        "lastContact25": self.user["lastContact25"]
                        })
            except:
                date = self.user['last_message'].replace('/','-')
                if self.diff_days(date) >= 1:
                    self.user_list.append({
                        "id":self.user['_id'],
                        "lastContact25": False
                        })
            
        return self.user_list